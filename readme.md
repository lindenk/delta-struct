Deltastruct
===========

A Rust library to define and serialize custom changes to a type. 

Features an attribute macro to generate a delta type and implementation for any (most) of your own structs. Deltas can then be applied to it's respective struct type, it's fields, or serialized/deserialized for later use.

## Example

```rust
use deltastruct::*;

#[deltastruct]
mod Vector3 {
  #[derive(Clone, Copy)]
  pub struct Vector3 {
    pub x : f64,
    pub y : f64,
    pub z : f64,
  }

  impl Vector3 {
    fn set(&mut self, other : Vector3) {
      // Copy the given vec's components into our own.
      self.x = other.x;
      self.y = other.y;
      self.z = other.z;
    }

    fn multiply_scalar(&mut self, n : f64) {
      // Multiply each component by the given value
      self.x *= n;
      self.y *= n;
      self.z *= n;
    }

    fn normalize(&mut self) {
      // divide each component by the vector's magnitude
      let magnitude = (
          self.x.powf(2f64) 
        + self.y.powf(2f64) 
        + self.z.powf(2f64)
      ).sqrt();
      
      self.x /= magnitude;
      self.y /= magnitude;
      self.z /= magnitude;
    }
  }
}

fn main() {
  let mut v = Vector3 {
    x: 3f64,
    y: 6f64,
    z: 6f64
  };


  let v_delta = Vector3Delta::MultiplyScalar(4f64);
  v.apply(&v_delta);
  v.apply(&v_delta);

  assert_eq!(v.x, 48f64);


  let v_delta = Vector3Delta::Normalize();
  v.apply(&v_delta);

  assert_eq!(v.z, 2f64/3f64)


  let v_delta = Vector3Delta::X(f64Delta::Set(8f64));
  v.apply(&v_delta);

  assert_eq!(v.x, 8f64);
}
```